

import UIKit

class ViewController2: UIViewController {
    @IBOutlet weak var MyCollectionView: UICollectionView!
    var Photos = [myPhoto]()
    var cache = NSCache<AnyObject, AnyObject>()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(String(describing: type(of: self)))")
        // Do any additional setup after loading the view.
    }
}
extension ViewController2: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Photos.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyPhotoCell
        let row = indexPath.row
        let target = Photos[row]
        cell.title.text = target.title
        cell.id.text = String(target.id)
//        cell.thumbnail.image = GetThumbnailImage(for: target)
        if let img = cache.object(forKey: target.thumbnailUrl as AnyObject){
            cell.thumbnail.image = (img as! UIImage)
        }else{
            DispatchQueue.global().async {
                do{
                    let dataThumbnail = try Data(contentsOf: URL(string: target.thumbnailUrl)!)
                    DispatchQueue.main.async {
                        let imgDownloaded = UIImage(data: dataThumbnail)
                        self.cache.setObject(imgDownloaded!, forKey: target.thumbnailUrl as AnyObject)
                        cell.thumbnail.image = imgDownloaded
                    }
                }catch let errorThumbnail{
                    print("Error found when downloading thumbnail \(target.id): \(errorThumbnail)")
                }
            }
        }
        return cell
    }
}

extension ViewController2: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = MyCollectionView.frame.size
        return CGSize(width: size.width/4, height: size.width/4)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension ViewController2 {
    func GetThumbnailImage(for targetPhoto: myPhoto) -> UIImage?{
        var imgThumbnail: UIImage?
        if let img = cache.object(forKey: targetPhoto.thumbnailUrl as AnyObject){
            imgThumbnail = (img as! UIImage)
        }else{
            DispatchQueue.global().async {
                do{
                    let dataThumbnail = try Data(contentsOf: URL(string:targetPhoto.thumbnailUrl)!)
                    DispatchQueue.main.async {
                        let imgDownloaded = UIImage(data: dataThumbnail)
                        imgThumbnail = imgDownloaded
                        self.cache.setObject(imgDownloaded!, forKey: targetPhoto.thumbnailUrl as AnyObject)
                    }
                }catch let errorThumbnail{
                    print("Error found when downloading thumbnail \(targetPhoto.id): \(errorThumbnail)")
                }
            }
        }
        return imgThumbnail
    }
}
