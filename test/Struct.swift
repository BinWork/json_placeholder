


struct myPhoto: Decodable{
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
    
    init(json: [String: Any]){
        albumId = json["albumId"] as? Int ?? -1
        id = json["id"] as? Int ?? -1
        title = json["title"] as? String ?? ""
        url = json["url"] as? String ?? ""
        thumbnailUrl = json["thumbnailUrl"] as? String ?? ""
    }
}
