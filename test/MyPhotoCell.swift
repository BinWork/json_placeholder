//
//  MyPhotoCell.swift
//  test
//
//  Created by nwfmbin2 on 2020/04/14.
//  Copyright © 2020 nwfm.work. All rights reserved.
//

import UIKit

class MyPhotoCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
}
