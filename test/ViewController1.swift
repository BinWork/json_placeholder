

import UIKit
import Alamofire

class ViewController1: UIViewController {
    private let strTargetLink = "https://jsonplaceholder.typicode.com/photos"
    @IBAction func actionRequest(_ sender: UIButton) {
        GetJSON(with: strTargetLink)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print(String(describing: type(of: self)))
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ViewController2,
            segue.identifier == "segueRequest" {
            vc.Photos = sender as! [myPhoto]
        }
    }
    
}

extension ViewController1{
    
    private func GetJSON(with sourceLink: String){
        print("enter \(#function)")
        guard let urlTarget = URL(string: sourceLink) else {return}
        let task = URLSession.shared.dataTask(with: urlTarget) {
            [weak self] (data, resposne, error) in
            guard let strongSelf = self else { return }
            guard error == nil else{
                print("error found in \(#function)")
                return
            }
            guard let Photos = strongSelf.DataToPhoto(withData: data!) else{
                print("error found in \(#function)")
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                strongSelf.performSegue(withIdentifier: "segueRequest", sender: Photos)
            })
        }
        task.resume()
    }
    
    private func DataToPhoto(withData data: Data) -> [myPhoto]?{
        do{
            let photos:[myPhoto] = try JSONDecoder().decode(Array<myPhoto>.self, from: data)
            return photos
        } catch let jsonError{
            print("JSON error: \(jsonError)")
        }
        return nil
    }
}
